//
//  ViewModelBindableType.swift
//  RxMemo
//
//  Created by 이민철 on 2020/12/11.
//

import UIKit

protocol ViewModelBindableType {
	
	//제네릭 타입으로 만든 변수타입
	//타입은 프로토콜을 상속 받는 구조체나 클래스가 정의한다.
	associatedtype ViewModelType
	
	var viewModel : ViewModelType! {get set}
	func bindViewModel()
}


extension ViewModelBindableType where Self: UIViewController {
	//해당 viewcontroller 와 viewmodel을 바인딩한다.
	
	mutating func bind(viewModel: Self.ViewModelType){
		self.viewModel = viewModel
		loadViewIfNeeded()
		
		bindViewModel()
	}
}
