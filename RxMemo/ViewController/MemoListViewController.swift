//
//  MemoListViewController.swift
//  RxMemo
//
//  Created by 이민철 on 2020/12/11.
//

import UIKit
import RxCocoa
import RxSwift
import NSObject_Rx
class MemoListViewController: UIViewController, ViewModelBindableType{
	
	var viewModel : MemoListViewModel!
	
	@IBOutlet weak var listTableView: UITableView!
	
	@IBOutlet weak var addBtn: UIBarButtonItem!
	
	override func viewDidLoad() {
        super.viewDidLoad()

	}
    

	func bindViewModel() {
		
		viewModel.title
			.drive(navigationItem.rx.title)
			.disposed(by: rx.disposeBag)
		
		viewModel.memoList
			.bind(to: listTableView.rx.items(dataSource: viewModel.dataSource))
			.disposed(by: rx.disposeBag)
		
		addBtn.rx.action = viewModel.makeCreakeAction()
		
		Observable.zip(listTableView.rx.modelSelected(Memo.self),
					   listTableView.rx.itemSelected)
			.do(onNext: { [unowned self] (_, indexPath) in
				self.listTableView.deselectRow(at: indexPath, animated: true)
			})
			.map { $0.0 }
			.bind(to: viewModel.detailAction.inputs)
			.disposed(by: rx.disposeBag)
		
		listTableView.rx.modelDeleted(Memo.self)
			.bind(to: viewModel.deleteAction.inputs)
			.disposed(by: rx.disposeBag)
		
	}

}
