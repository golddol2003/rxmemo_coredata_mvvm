//
//  CommonViewModel.swift
//  RxMemo
//
//  Created by 이민철 on 2020/12/11.
//

import Foundation
import RxSwift
import RxCocoa

class CommonViewModel: NSObject {
	
	let title : Driver<String>
	let sceneCoordinator : SceneCoordinatorType
	let storage : MemmoStorageType
	
	init(title: String, sceneCoordinator: SceneCoordinatorType, storage : MemmoStorageType) {
		self.title = Observable.just(title).asDriver(onErrorJustReturn: "")
		self.sceneCoordinator = sceneCoordinator
		self.storage = storage
	}
}
