//
//  TransitionModel.swift
//  RxMemo
//
//  Created by 이민철 on 2020/12/11.
//

import Foundation

enum TransitionStyle {
	case root
	case push
	case modal
}

enum TransitionError : Error {
	case navigationControllerMissing
	case cannotPop
	case unknown
}
