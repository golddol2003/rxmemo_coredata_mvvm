//
//  SceneCoordinatorType.swift
//  RxMemo
//
//  Created by 이민철 on 2020/12/11.
//

import Foundation
import RxSwift

protocol SceneCoordinatorType {
	@discardableResult
	func transition(to scene : Scene , using style : TransitionStyle,  animated: Bool)
	  -> Completable
	
	@discardableResult
	func close(animated: Bool) -> Completable

}
