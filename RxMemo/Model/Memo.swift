//
//  Memo.swift
//  RxMemo
//
//  Created by 이민철 on 2020/12/11.
//

import Foundation
import RxDataSources
import CoreData
import RxCoreData

struct Memo : Equatable, IdentifiableType{
	
	var content : String
	var inserDate : Date //생성 날짜
	var identity : String //메모 구분
	
	init(content:String, inserDate: Date = Date() ) {
		self.content = content
		self.inserDate = inserDate
		self.identity = "\(inserDate.timeIntervalSinceReferenceDate)"
	}
	init(original : Memo, updatedContent: String) {
		self = original
		self.content = updatedContent
	}
}

extension Memo: Persistable {
	public static var entityName: String {
		return "Memo"
	}
	
	static var primaryAttributeName: String {
		return "identity"
	}
	
	init(entity: NSManagedObject) {
		content = entity.value(forKey: "content") as! String
		inserDate = entity.value(forKey: "insertDate") as! Date
		identity = "\(inserDate.timeIntervalSinceReferenceDate)"
	}
	func update(_ entity: NSManagedObject) {
		entity.setValue(content, forKey: "content")
		entity.setValue(inserDate, forKey: "insertDate")
		entity.setValue("\(inserDate.timeIntervalSinceReferenceDate)", forKey: "identity")
		
		do {
			try entity.managedObjectContext?.save()
		} catch {
			print(error)
		}
	}
}
