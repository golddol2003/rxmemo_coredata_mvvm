//
//  MemoStorageType.swift
//  RxMemo
//
//  Created by 이민철 on 2020/12/11.
//

import Foundation
import RxSwift
protocol  MemmoStorageType{
	//	@discardableResult : 작업결과가 필요 없을수도 있기 때문에
	@discardableResult
	func createMemo(content: String) -> Observable<Memo>
	
	@discardableResult
	func memoList() -> Observable<[MemoSectionModel]>
	
	@discardableResult
	func update(memo: Memo, content: String) -> Observable<Memo>
	
	@discardableResult
	func delete(memo: Memo) -> Observable<Memo>
}
