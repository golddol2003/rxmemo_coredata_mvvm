//
//  CoreDataStorage.swift
//  RxMemo
//
//  Created by 이민철 on 2020/12/14.
//

import Foundation
import RxSwift
import RxCoreData
import CoreData

class CoreDataStorage: MemmoStorageType {
	//공통으로 쓰일 모델네임
	let modelName : String
	//모델네임 이름을 초기화
	init(modelName:String){
		self.modelName = modelName
	}
	
	private lazy var persistentContainer: NSPersistentContainer = {
		let container = NSPersistentContainer(name: modelName)
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error as NSError? {
				
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
		})
		return container
	}()
	private var mainContext: NSManagedObjectContext{
		return persistentContainer.viewContext
	}
	
	func createMemo(content: String) -> Observable<Memo> {
		let memo = Memo(content: content)
		
		do {
			//upsert 방식
			//기존에 있다면 update , 없다면 insert
			_ = try mainContext.rx.update(memo)
			return Observable.just(memo)
		} catch {
			return Observable.error(error)
		}
	}
	
	func memoList() -> Observable<[MemoSectionModel]> {
		//(메모타입, sort 방식).map(datasource 형식에 맞춰서 방출한다.)
		return mainContext.rx.entities(Memo.self, sortDescriptors: [NSSortDescriptor(key: "insertDate", ascending: false)])
			.map{ results in [MemoSectionModel(model: 0, items: results)]}
	}
	
	func update(memo: Memo, content: String) -> Observable<Memo> {
		let updated = Memo(original: memo, updatedContent: content)
		
		do {
			_ = try mainContext.rx.update(updated)
			return Observable.just(updated)
		} catch {
			return Observable.error(error)
		}
	}
	
	func delete(memo: Memo) -> Observable<Memo> {
		do {
			try mainContext.rx.delete(memo)
			return Observable.just(memo)
		} catch {
			return Observable.error(error)
		}
	}
	
}
